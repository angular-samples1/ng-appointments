import { Injectable } from '@angular/core';
import { v4 as uuidv4 } from 'uuid';
import { Appointment } from '../models/appointment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AppointmentService {
  constructor() {}

  createNewAppointment(appointment: Appointment) {
    let currentAppointments: Appointment[] = JSON.parse(
      localStorage.getItem('appointments') || '[]'
    );

    appointment.appointmentId = uuidv4();
    currentAppointments.push(appointment);

    localStorage.setItem('appointments', JSON.stringify(currentAppointments));
  }

  getAppointments(): Observable<Appointment[]> {
    const appointments: Appointment[] = JSON.parse(
      localStorage.getItem('appointments') || '[]'
    );
    return new Observable((observer) => observer.next(appointments));
  }

  getAppointmentDetails(appointmentId: string) {
    let currentAppointments: Appointment[] = JSON.parse(
      localStorage.getItem('appointments') || '[]'
    );

    return currentAppointments.find((x) => x.appointmentId === appointmentId);
  }

  updateAppointment(appointment: Appointment) {
    let currentAppointments: Appointment[] = JSON.parse(
      localStorage.getItem('appointments') || '[]'
    );

    let index = currentAppointments.findIndex(
      (x) => x.appointmentId === appointment.appointmentId
    );

    currentAppointments[index] = appointment;

    localStorage.setItem('appointments', JSON.stringify(currentAppointments));
  }

  deleteAppointment(appointmentId: string) {
    let currentAppointments: Appointment[] = JSON.parse(
      localStorage.getItem('appointments') || '[]'
    );

    let index = currentAppointments.findIndex(
      (x) => x.appointmentId === appointmentId
    );

    currentAppointments.splice(index, 1);

    localStorage.setItem('appointments', JSON.stringify(currentAppointments));
  }
}
