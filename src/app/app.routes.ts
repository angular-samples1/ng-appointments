import { Routes } from '@angular/router';
import { AppointmentEditComponent } from './components/appointment-edit/appointment-edit.component';
import { AppointmentListComponent } from './components/appointment-list/appointment-list.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

export const routes: Routes = [
  { path: 'list', component: AppointmentListComponent },
  { path: 'new', component: AppointmentEditComponent },
  { path: 'edit/:id', component: AppointmentEditComponent },
  { path: '', component: AppointmentListComponent, pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];
