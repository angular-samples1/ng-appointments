export class Appointment {
  appointmentId: string | null | undefined;
  appointmentDate: string | null | undefined;
  appointmentDescription: string | null | undefined;
}
