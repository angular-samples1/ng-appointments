import { Component } from '@angular/core';
import { ReactiveFormsModule, Validators } from '@angular/forms';
import { FormGroup, FormControl } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { CalendarModule } from 'primeng/calendar';
import { InputGroupModule } from 'primeng/inputgroup';
import { InputGroupAddonModule } from 'primeng/inputgroupaddon';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { AppointmentService } from '../../services/appointment.service';
import { Appointment } from '../../models/appointment';

@Component({
  selector: 'app-appointment-edit',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    InputTextModule,
    CalendarModule,
    InputGroupModule,
    InputGroupAddonModule,
    CardModule,
    ButtonModule,
  ],
  templateUrl: './appointment-edit.component.html',
  styleUrl: './appointment-edit.component.css',
})
export class AppointmentEditComponent {
  constructor(private appointmentService: AppointmentService) {}

  appointmentForm = new FormGroup({
    appointmentDescription: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
    ]),
    appointmentDate: new FormControl('', [Validators.required]),
  });

  onSubmit(): void {
    // console.log(this.appointmentForm.value);
    const appointment = new Appointment();
    appointment.appointmentDescription =
      this.appointmentForm.value.appointmentDescription;
    appointment.appointmentDate = this.appointmentForm.value.appointmentDate;
    console.log(appointment);
    this.appointmentService.createNewAppointment(appointment);
  }
}
