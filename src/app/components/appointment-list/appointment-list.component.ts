import { Component, OnDestroy, OnInit } from '@angular/core';
import { TableModule } from 'primeng/table';
import { AppointmentService } from '../../services/appointment.service';
import { Appointment } from '../../models/appointment';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-appointment-list',
  standalone: true,
  imports: [TableModule, DatePipe],
  templateUrl: './appointment-list.component.html',
  styleUrl: './appointment-list.component.css',
})
export class AppointmentListComponent implements OnInit, OnDestroy {
  appointments!: Appointment[];
  styleClass: string = 'p-datatable-striped p-datatable-lg';
  private getAppointments$: any;

  constructor(private appointmentService: AppointmentService) {}

  ngOnInit(): void {
    this.getAppointments$ = this.appointmentService
      .getAppointments()
      .subscribe({
        next: (data: Appointment[]) => {
          if (data.length === 0) {
            this.appointments = [];
            return;
          }

          this.appointments = data;
          console.log(this.appointments);
        },
        error: (err: any) => {
          console.warn(err);
        },
      });
  }

  ngOnDestroy(): void {
    this.getAppointments$.unsubscribe();
  }

  onAppointmentSelect(selectedAppointment: Appointment) {
    console.log(selectedAppointment);
    // TODO: navigate to appointment details
    console.log('Navigate to appointment details');
  }
}
