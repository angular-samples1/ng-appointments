import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { MenubarModule } from 'primeng/menubar';
import { MenuItem } from 'primeng/api';
import { PrimeNGConfig } from 'primeng/api';
import { AppointmentListComponent } from './components/appointment-list/appointment-list.component';

@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
  imports: [RouterOutlet, AppointmentListComponent, MenubarModule],
})
export class AppComponent implements OnInit {
  items: MenuItem[] | undefined;

  constructor(private primengConfig: PrimeNGConfig) {}

  ngOnInit(): void {
    this.primengConfig.ripple = true;
    this.items = [
      {
        label: 'Home',
        icon: 'pi pi-fw pi-home',
        routerLink: ['/'],
      },
      {
        label: 'Appointments',
        icon: 'pi pi-fw pi-calendar',
        items: [
          {
            label: 'List',
            icon: 'pi pi-fw pi-list',
            routerLink: ['/list'],
          },
          {
            label: 'New',
            icon: 'pi pi-fw pi-plus',
            routerLink: ['/new'],
          },
        ],
      },
    ];
  }
}
